import sys
import nltk
import csv
import re

input_file = "data/cordis-h2020reports.csv"
input_file2 = "data/cordis-h2020projects.csv"
output_file = "existing_acronyms.txt"


def read_file(filepath, fn, delimiter=';', quotechar='"', newline=''):
    with open(filepath, newline=newline) as csvfile:
        r = csv.DictReader(
            csvfile,
            delimiter=delimiter,
            quotechar=quotechar,
        )
        for row in r:
            yield fn(row)


def __parse_row(row):
    if 'acronym' in row.keys():
        return row['acronym']
    return row['projectAcronym']


def export_acronym():

    all_acronyms = {}
    for acronym in read_file(input_file, __parse_row):
        all_acronyms.setdefault(acronym, 0)
        all_acronyms[acronym] += 1

    for acronym in read_file(input_file2, __parse_row):
        all_acronyms.setdefault(acronym, 0)
        all_acronyms[acronym] += 1

    with open(output_file, 'w') as w:
        for acronym in all_acronyms:
            acronym = re.sub(r'[0-9._-]', '', acronym)
            acronym = acronym.strip()
            if len(acronym) < 6:
                continue
            if " " in acronym:
                continue
            w.write(f"{acronym}\n")


if __name__ == '__main__':
    export_acronym()
