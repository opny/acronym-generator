
window.addEventListener('load', function() {

  const upper = (title, tag="b") => {
    let html = ""
    for (var i = 0; i < title.length; i++) {
      let chr = title[i]
      if (chr === chr.toUpperCase()) {
        chr = `<${tag}>${chr}</${tag}>`
      }
      html += chr
    }
    return html
  }

  const query = document.getElementById("query")
  const results = document.getElementById("results")
  const no_results = document.getElementById("no_results")
  const working = document.getElementById("working")

  let _lock = false

  const lock = () => {
    _lock = true
    query.parentNode.classList.toggle('is-loading')
    working.style.display = "block"
  }

  const unlock = () => {
    _lock = false
    query.parentNode.classList.toggle('is-loading')
    working.style.display = "none"
  }

  query.addEventListener('keypress', function(ev) {

    // enter
    if (ev.charCode !== 13) {
      return
    }

    if (_lock) {
      console.log("request locked");
      return
    }

    lock()

    results.innerHTML = ''

    fetch('/acronym?q=' + query.value)
      .then(response => {
        if (!response.ok) {
          unlock()
          throw new Error("Network error")
        }
        return response.json()
      })
      .then(json => {
        // console.warn(json);

        const keys = Object.keys(json)

        // show no results
        no_results.style.display = (keys.length === 0) ? "block" : "none"
        keys
          .map(acronym => `
            <span class="tag is-large acronym">${acronym}</span>
            <span class="separator"> </span>
            <span class="query">${upper(json[acronym], 'span')}</span>
          `)
          .forEach(row => {
            const li = document.createElement('li')
            li.innerHTML = row
            results.appendChild(li)
          })

        unlock()
      })
      .catch((e) => {
        console.error(e)
        unlock()
      })

  })

  working.style.display = "none"
  console.log("ready")
})
