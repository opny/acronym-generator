FROM python:3.7

RUN mkdir /app
WORKDIR /app
COPY requirements.txt /app
COPY serve.py /app
COPY main.py /app
COPY dataset.py /app
COPY public /app/public
COPY Makefile /app
COPY existing_acronyms.txt /app

RUN pip3 install -r requirements.txt

RUN python3 -m nltk.downloader words

ENTRYPOINT ["python3", "serve.py"]
