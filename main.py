from acronym import acronym
import nltk
import sys
import os
import argparse


def get_acronyms(name, existing_path="./"):

    a = {}

    b = __get_acronyms(name, nltk.corpus.words, existing_path)
    a = {**a, **b}

    b = __get_acronyms(name, nltk.corpus.brown, existing_path)
    a = {**a, **b}

    b = __get_acronyms(name, nltk.corpus.gutenberg, existing_path)
    a = {**a, **b}

    keys = sorted(a.keys())
    keys.sort(key=len, reverse=True)

    list = {}
    for k in keys:
        list[k] = a[k]

    return list


def __get_acronyms(name, corpus=nltk.corpus.gutenberg, existing_path="./"):

    # corpus = nltk.corpus.words
    # corpus = nltk.corpus.brown
    # corpus = nltk.corpus.gutenberg

    existing = {}
    existing_path = os.path.join(existing_path, 'existing_acronyms.txt')
    # print(existing_path)
    with open(existing_path, 'r') as f:
        for line in f.readlines():
            line = line.strip('\n')
            key, _, val = line.partition(': ')
            existing[key.lower()] = val.lower()

    results = acronym.find_acronyms(
        name,
        corpus,
        # existing=existing,
        min_length=6,
        max_length=12
    )

    keys = sorted(results.keys())
    keys.sort(key=len, reverse=True)

    for k in keys:
        if k.lower() in existing.keys():
            del results[k]

    return results


if __name__ == '__main__':

    formatter = argparse.ArgumentDefaultsHelpFormatter
    parser = argparse.ArgumentParser(formatter_class=formatter)
    parser.add_argument('name', metavar='ProjectName', type=str,
                        help='the name to generate acronyms from')
    args = parser.parse_args()


    f = sys.stdout

    list = get_acronyms(args.name)
    for acronym in list.keys():
        f.write(f'{acronym:12} {list[acronym]}\n')
        f.flush()
