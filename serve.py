#!/usr/bin/env python

from main import get_acronyms
from http.server import (
    HTTPServer as BaseHTTPServer,
    SimpleHTTPRequestHandler
)
from urllib.parse import parse_qs, urlparse
import json
import os
import logging

logging.basicConfig(
    level=logging.INFO,
    filename='data/queries.log',
    filemode='w',
    format='%(asctime)s %(message)s',
)


class HTTPRequestHandler(SimpleHTTPRequestHandler):

    """This handler uses server.base_path instead of
        always using os.getcwd()"""
    def translate_path(self, path):
        path = SimpleHTTPRequestHandler.translate_path(self, path)
        relpath = os.path.relpath(path, os.getcwd())
        fullpath = os.path.join(self.server.base_path, relpath)
        return fullpath

    def generate_acronym(self):

        # Send response status code
        self.send_response(200)

        # Send headers
        self.send_header('Content-type', 'application/json')
        self.end_headers()

        url_path = urlparse(self.path)
        query = url_path.query

        if len(query) < 2:
            self.wfile.write(bytes("{}", "utf8"))
            return

        if len(query) > 128:
            query = query[:128]

        query_components = dict(qc.split("=") for qc in query.split("&"))
        qs = parse_qs(urlparse(self.path).query)

        q = qs.get('q', None)

        if q is None or len(q) == 0:
            self.wfile.write(bytes("{}", "utf8"))
            return

        q = q[0]
        logging.info(f"Query: {q}")

        list = get_acronyms(q)
        self.wfile.write(bytes(json.dumps(list), "utf8"))
        return

    # GET
    def do_GET(self):
        url_path = urlparse(self.path)
        if url_path.path == "/acronym":
            return self.generate_acronym()

        return super().do_GET()


class HTTPServer(BaseHTTPServer):
    """The main server, you pass in base_path which is the path you want to
        serve requests from"""
    def __init__(
        self, base_path, server_address, RequestHandlerClass=HTTPRequestHandler
    ):
        self.base_path = base_path
        BaseHTTPServer.__init__(self, server_address, RequestHandlerClass)


def run():

    logging.debug('starting server...')

    web_dir = os.path.join(os.path.dirname(__file__), 'public')
    port = 8081
    httpd = HTTPServer(web_dir, ("", port))

    logging.debug(f'running server on {port}...')
    httpd.serve_forever()


if __name__ == '__main__':
    run()
