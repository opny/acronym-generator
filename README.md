# acronym-generator

A service to generate acronyms. Based on [bacook17/acronym](https://github.com/bacook17/acronym).

Read more on [that post](https://opny.cc/d/b174t1bm4r)

### License

Apache 2.0

Copyright 2019 luca capra

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
