
PYEXE ?= ./venv/bin/python3

download:
	mkdir -p data
	cd data && wget https://cordis.europa.eu/data/cordis-h2020reports.csv
	cd data && wget https://cordis.europa.eu/data/cordis-h2020projects.csv


setup:
	virtualenv ./venv -p python3
	./venv/bin/pip install -r requirements.txt

cli:
	${PYEXE} main.py

serve:
	${PYEXE} serve.py

export_acronym:
	${PYEXE} dataset.py

docker/build:
	docker build . -t opny/acronym-generator:latest

docker/push:
	docker push opny/acronym-generator:latest

docker/run:
	docker run --rm -p 8081:8081 -v `pwd`/data:/app/data opny/acronym-generator:latest
